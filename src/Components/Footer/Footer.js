import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import "./Footer.css";

class Footer extends Component {
  render() {
    console.log("Omer Yildirim wishes you healthy days");
    return (
      <div
        style={{
          boxSizing: "border-box",
          padding: 10,
          borderTop: "1px solid lightgray",
          height: 100,
          backgroundColor: "#fafafa",
          justifyContent: "space-around",
          display: "flex"
        }}
      >
        <div>
          <div
            style={{ color: "#504F5A", fontWeight: "bold", marginBottom: 10 }}
          >
            Contact
          </div>
          <NavLink
            to={"/"}
            exact
            style={{
              textDecoration: "none",
              color: "#000"
            }}
          >
            <div className="footerItem">Customer Satisfaction</div>
          </NavLink>
        </div>
        <div>
          <div
            style={{ color: "#504F5A", fontWeight: "bold", marginBottom: 10 }}
          >
            About us
          </div>
          <NavLink
            to={""}
            exact
            style={{
              textDecoration: "none",
              color: "#000"
            }}
          >
            <div className="footerItem">Info</div>
          </NavLink>
        </div>
        <div>
          <div
            style={{ color: "#504F5A", fontWeight: "bold", marginBottom: 10 }}
          >
            Social Media
          </div>
            <div className="footerItem">LinkedIn</div>
        </div>
      </div>
    );
  }
}

export default Footer;