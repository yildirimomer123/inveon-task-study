import React, { Component } from "react";
import IconButton from "@material-ui/core/IconButton";
import AddShoppingCartIcon from "@material-ui/icons/AddShoppingCart";
import { connect } from "react-redux";
import { addItemInCart } from "../../Redux/Actions";
import { withRouter } from "react-router-dom";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Tooltip from "@material-ui/core/Tooltip";
import Button from "@material-ui/core/Button";

class ConnectedItem extends Component {
  render() {
    return (
      <Card
        style={{ width: 280, height: 400, margin: 15, display: "inline-block" }}
      >
        <CardActionArea
          onClick={() => {
            this.props.history.push("#");
          }}
        >
          <CardMedia
            style={{ height: 260 }}
            image={this.props.item.imageUrls[0]}
          />
          <CardContent style={{ height: 50, fontSize: 13, lineHeight: 1.5 }}>
            <div
              style={{
                marginLeft: 0,
                fontWeight: "bold",
                whiteSpace: "nowrap",
                overflow: "hidden",
                textOverflow: "ellipsis"
              }}
            >
              {this.props.item.name}
            </div>
            <div style={{ margin: 0 }}>Price: {this.props.item.price} $</div>
            <div style={{ color: "#64bac8", fontWeight: "bold", margin: 0 }}>
              {this.props.item.popular && "Popular"}
            </div>
          </CardContent>
        </CardActionArea>
        <CardActions
          style={{ display: "flex", alignItems: "center", height: 45 }}
        >
          <Button
            size="small"
            variant="outlined"
            style={{ marginRight: 140, marginLeft:10 }}
            onClick={() => {
              this.props.history.push("#");
            }}
          >
            {" "}
            Details
          </Button>
          <Tooltip title="Add to cart">
            <IconButton
              size="small"
              onClick={e => {
                e.stopPropagation();
                this.props.dispatch(
                  addItemInCart({ ...this.props.item, quantity: 1 })
                );
              }}
              color="secondary"
              aria-label="Add to shopping cart"
            >
              <AddShoppingCartIcon size="small" />
            </IconButton>
          </Tooltip>
        </CardActions>
      </Card>
    );
  }
}

export default withRouter(connect()(ConnectedItem));
