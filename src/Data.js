/*---- Product db ----*/
const inveonProducts = [
  {
    id: 1,
    name: "Inveon Omnichannel Commerce",
    category: "E-Commerce",
    price: 999,
    description:
      "On a single platform, integrate online and brick-and-mortar stores, manage and optimize your operations, and offer a seamless customer journey.",
    popular: true,
    imageUrls: [
      "https://i2.wp.com/www.inveon.com/wp-content/uploads/2016/01/smartmockups_k8hipgls-1024x650.png"
    ]
  },
  {
    id: 2,
    name: "Inveon App-Commerce",
    category: "E-Commerce",
    price: 699,
    description:
      "The app-commerce platform, Mobular, provides your customers with a seamless mobile brand experience.", 

    popular: true,
    imageUrls: [
      "https://i2.wp.com/www.inveon.com/wp-content/uploads/2020/03/smartmockups_k8slfi8b.png"
    ]
  },
  {
    id: 3,
    name: "Inveon Order Management System (OMS)",
    category: "E-Commerce",
    price: 599,
    description:
      "Flawless fulfillment of orders from all channels",

    popular: true,
    imageUrls: [
      "https://i2.wp.com/www.inveon.com/wp-content/uploads/2016/01/smartmockups_k8az7xrw-1024x554.png"
    ]
  },
  {
    id: 4,
    name: "Inveon Marketplace Integrator",
    category: "E-Commerce",
    price: 4199,
    description: 
      "Integrating to all marketplaces to allow you to manage your channel presence with ease through a single platform.",
    popular: false,
    imageUrls: [
      "https://i2.wp.com/www.inveon.com/wp-content/uploads/2019/09/inveon_logo.png"
    ]
  },
  {
    id: 5,
    name: "Growth Management Solutions",
    category: "Analytics",
    price: 399,
    description: "",
    popular: false,
    imageUrls: [
      "https://i2.wp.com/www.inveon.com/wp-content/uploads/2020/03/smartmockups_k8h5dhnb-1024x845.png"
    ]
  }
];

/*----- prodCategories -----*/
const prodCategories = [
  {
    name: "All Products",
    icon: "list"
  },
  {
    name: "E-Commerce",
    icon: "payments"
  },
  {
    name: "Analytics",
    icon: "trending_up"
  }
];

/*---- Rendering Menu data ----*/
const menuData = [
  { name: "Home page", url: "/", icon: "home", id: 0 },
  {
    name: "Product Categories",
    id: 1,
    children: prodCategories.map((x, i) => {
      return {
        name: x.name,
        id: i,
        url: "/?category=" + x.name,
        icon: x.icon
      };
    })
  }
];

export { inveonProducts, prodCategories, menuData };